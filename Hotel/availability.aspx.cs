﻿using System;
using System.Data.SqlClient;

public partial class Ledighed : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        for (int i = 1; i < 54; i++)
        {
            DropDownList_uge.Items.Add(i.ToString());
        }

        for (int i = 2015; i < 2017; i++)
        {
            DropDownList_år.Items.Add(i.ToString());
        }
        
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("Frontpage.aspx");
    }
    protected void Button1_Click1(object sender, EventArgs e)
    {
        btn_ok.Visible = false;
        Label_Warning.Text = "";
        
        if (!CheckAvailability())
            Label_Warning.Text = "Sommerhuset er allerede reserveret i den valgte uge!";
        else
        {
            Label_Warning.Text = "Ugen de har valgt er ledig, ønsker de at reservere?";
            btn_ok.Visible = true;
        }
    }

    private bool CheckAvailability()
    {
        SqlDataReader reader;
        string connstring = SqlDataSourceTilAvailability.ConnectionString;
        SqlConnection cnn = new SqlConnection(connstring);
        cnn.Open();
        string SqlCommand = string.Format("Select Udlejet from dbo.Lejer where År = {0} AND UgeNr = {1}"
            , DropDownList_år.SelectedItem, DropDownList_uge.SelectedItem);
        SqlCommand myCommand = new SqlCommand(SqlCommand, cnn);
        reader = myCommand.ExecuteReader();
        while (reader.Read())
            if ((bool) reader.GetValue(0))
                return false;
            else
            {
                return true;
            }
        return false;
    }
    protected void btn_ok_Click(object sender, EventArgs e)
    {
        if (!CheckAvailability())
            Button1_Click1(sender, e);
        else
        {
            Session.Add("Week", DropDownList_uge.SelectedItem);
            Session.Add("Year", DropDownList_år.SelectedItem);
            Response.Redirect("Reservation.aspx");
        }
    }
}