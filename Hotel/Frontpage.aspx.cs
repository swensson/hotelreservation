﻿using System;

public partial class Frontpage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Resevated"] != null)
        {
            string msg = string.Format("Der blev oprettet en reservation med følgende oplysninger: \\nNavn: {0} \\nAdresse: {1} \\nTelefonNr: {2} \\nUge {3} År {4}", 
                            Session["Name"], Session["Adress"], Session["Phone"],Session["Week"], Session["Year"]);
            Response.Write(String.Format(@"<script>alert('" + msg + "');</script>"));
            Session["Resevated"] = null;
        }
    }

    protected void Lbtn_Ledig_Click(object sender, EventArgs e)
    {
        Response.Redirect("availability.aspx");
    }
    protected void Lbtn_reservation_Click(object sender, EventArgs e)
    {
        Response.Redirect("Reservation.aspx");
    }
    protected void Lbtn_alleReservationer_Click(object sender, EventArgs e)
    {
        Response.Redirect("AllReservations.aspx");
    }
}