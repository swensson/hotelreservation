﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Reservation.aspx.cs" Inherits="Reservation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <h3>Reservation</h3>
    
    </div>
        <p>
            <asp:Label ID="Label1" runat="server" style="z-index: 1; left: 10px; top: 54px; position: absolute" Text="Navn:"></asp:Label>
            <asp:TextBox ID="txt_name" runat="server" style="z-index: 1; left: 100px; top: 54px; position: absolute"></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="Label2" runat="server" style="z-index: 1; left: 10px; position: absolute; top: 92px;" Text="Adresse:"></asp:Label>
            <asp:TextBox ID="txt_addr" runat="server" style="z-index: 1; left: 100px; top: 92px; position: absolute"></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="Label3" runat="server" style="z-index: 1; left: 10px; top: 130px; position: absolute; bottom: 401px;" Text="TelefonNr:"></asp:Label>
            <asp:TextBox ID="txt_phone" runat="server" style="z-index: 1; left: 100px; top: 130px; position: absolute"></asp:TextBox>
        </p>
    <p>
        <asp:Button ID="Btn_Update" runat="server" OnClick="Btn_Update_Click" style="z-index: 1; left: 10px; top: 168px; position: absolute" Text="Færdiggør reservation" />
        <asp:Label ID="label_Warning" runat="server" style="z-index: 1; left: 10px; top: 209px; position: absolute"></asp:Label>
        </p>
    <p>
        &nbsp;</p>
        <asp:Label ID="label_date" runat="server" style="z-index: 1; left: 300px; top: 53px; position: absolute"></asp:Label>
    <p>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SommerhusUdlejningConnectionString %>" DeleteCommand="DELETE FROM [Lejer] WHERE [UgeNr] = @original_UgeNr AND [År] = @original_År" InsertCommand="INSERT INTO [Lejer] ([UgeNr], [År], [Navn], [Adresse], [TelefonNr], [Dato], [Udlejet]) VALUES (@UgeNr, @År, @Navn, @Adresse, @TelefonNr, @Dato, @Udlejet)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [Lejer]" UpdateCommand="UPDATE [Lejer] SET [Navn] = @Navn, [Adresse] = @Adresse, [TelefonNr] = @TelefonNr, [Dato] = @Dato, [Udlejet] = @Udlejet WHERE [UgeNr] = @original_UgeNr AND [År] = @original_År">
            <DeleteParameters>
                <asp:Parameter Name="original_UgeNr" Type="Int32" />
                <asp:Parameter Name="original_År" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="UgeNr" Type="Int32" />
                <asp:Parameter Name="År" Type="Int32" />
                <asp:Parameter Name="Navn" Type="String" />
                <asp:Parameter Name="Adresse" Type="String" />
                <asp:Parameter Name="TelefonNr" Type="String" />
                <asp:Parameter Name="Dato" Type="DateTime" />
                <asp:Parameter Name="Udlejet" Type="Boolean" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Navn" Type="String" />
                <asp:Parameter Name="Adresse" Type="String" />
                <asp:Parameter Name="TelefonNr" Type="String" />
                <asp:Parameter Name="Dato" Type="DateTime" />
                <asp:Parameter Name="Udlejet" Type="Boolean" />
                <asp:Parameter Name="original_UgeNr" Type="Int32" />
                <asp:Parameter Name="original_År" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        </p>
    </form>
    </body>
</html>
