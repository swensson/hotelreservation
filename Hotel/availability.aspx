﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="availability.aspx.cs" Inherits="Ledighed" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #Select1 {
            z-index: 1;
            left: 449px;
            top: 138px;
            position: absolute;
        }
        #Select2 {
            z-index: 1;
            left: 547px;
            top: 137px;
            position: absolute;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <h3>Ledige uger<asp:Label ID="Label_Warning" runat="server" style="z-index: 1; left: 553px; top: 199px; position: absolute"></asp:Label>
        </h3>
    
    </div>
        <p>
            &nbsp;</p>
        <h4>Alle ledige uger:</h4>

        <asp:SqlDataSource ID="SqlDataSourceTilAvailability" runat="server" ConnectionString="<%$ ConnectionStrings:SommerhusUdlejningConnectionString %>" SelectCommand="SELECT [UgeNr], [År] FROM [Lejer] WHERE ([Udlejet] = @Udlejet) ORDER BY [År], [UgeNr]">
            <SelectParameters>
                <asp:Parameter DefaultValue="False" Name="Udlejet" Type="Boolean" />
            </SelectParameters>
        </asp:SqlDataSource>

        <asp:DataList ID="DataList1" runat="server" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" DataKeyField="UgeNr" DataSourceID="SqlDataSourceTilAvailability" GridLines="Horizontal" style="z-index: 1; left: 18px; top: 138px; position: absolute; height: 63px; width: 381px" ToolTip="Data">
            <FooterStyle BackColor="White" ForeColor="#333333" />
            <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
            <ItemStyle BackColor="White" ForeColor="#333333" />
            <ItemTemplate>
                UgeNr:
                <asp:Label ID="UgeNrLabel" runat="server" Text='<%# Eval("UgeNr") %>' />
                <br />
                År:
                <asp:Label ID="ÅrLabel" runat="server" Text='<%# Eval("År") %>' />
                <br />
<br />
            </ItemTemplate>
            <SelectedItemStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
        </asp:DataList>
        <asp:Button ID="btn_tilbage" runat="server" OnClick="Button1_Click" style="z-index: 1; left: 10px; top: 65px; position: absolute" Text="&lt;-- Tilbage" />
        <asp:Button ID="btn_reserver" runat="server" OnClick="Button1_Click1" style="z-index: 1; left: 446px; top: 199px; position: absolute" Text="Reservér" />
        <asp:Label ID="Label2" runat="server" style="z-index: 1; left: 561px; top: 113px; position: absolute" Text="År:"></asp:Label>
        <asp:Label ID="Label1" runat="server" style="z-index: 1; left: 446px; top: 113px; position: absolute; right: 577px" Text="Uge:"></asp:Label>
        <asp:DropDownList ID="DropDownList_år" runat="server" style="z-index: 1; left: 561px; top: 140px; position: absolute; height: 25px; width: 56px">
        </asp:DropDownList>
        <asp:DropDownList ID="DropDownList_uge" runat="server" style="z-index: 1; left: 446px; top: 140px; position: absolute; height: 25px; width: 56px; ">
        </asp:DropDownList>

    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>

        <asp:Button ID="btn_ok" runat="server" style="z-index: 1; left: 446px; top: 237px; position: absolute; height: 26px" Text="Ok" Visible="False" OnClick="btn_ok_Click" />

    </form>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
</body>
</html>
