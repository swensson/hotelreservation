﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reservation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        label_date.Text = "År " + Session["Year"] + " Uge " + Session["Week"];
        HttpCookie cookie = Request.Cookies.Get("Haislund&Swensson");
        if (cookie != null)
        {
            txt_name.Text = cookie["Name"];
            txt_addr.Text = cookie["Adress"];
            txt_phone.Text = cookie["Phone"];
        }
    }
    protected void Btn_Update_Click(object sender, EventArgs e)
    {
        if (txt_addr.Text != "" && txt_addr.Text.Length < 51 &&
            txt_name.Text != "" & txt_name.Text.Length < 11 &&
            txt_phone.Text != "" && txt_phone.Text.Length == 8)
        {
            CreateCookie();


            SqlDataSource1.UpdateCommand = String.Format(@"UPDATE Lejer
                                        SET Navn = '{0}', Adresse = '{1}', TelefonNr = {2}, Dato = GetDate(),  Udlejet = 1
                                        WHERE UgeNr = {3} AND År = {4}",
                    txt_name.Text, txt_addr.Text, txt_phone.Text,
                    Int32.Parse(Session["Week"].ToString()), Int32.Parse(Session["Year"].ToString()));
            try
            {
                SqlDataSource1.Update();
                Session["Name"] = txt_name.Text;
                Session["Adress"] = txt_addr.Text;
                Session["Phone"] = txt_phone.Text;
                Session["Resevated"] = txt_name.Text;
                Response.Redirect("Frontpage.aspx");   
            }
            catch (Exception ee)
            {
                txt_name.Text = ee.Message;
            }
        }
        else
        {
            label_Warning.Text = @"Felterne må ikke være tomme 
                                    & Navn max 10 tegn 
                                    & Tlf kun 8 tegn 
                                    & Adresse max 50 tegn";
        }
    }

    public void CreateCookie()
    {
        HttpCookie cookie = new HttpCookie("Haislund&Swensson");
        cookie.Expires = DateTime.UtcNow.AddDays(30);
        cookie["Name"] = txt_name.Text;
        cookie["Adress"] = txt_addr.Text;
        cookie["Phone"] = txt_phone.Text;
        Response.Cookies.Add(cookie);
    }
}