USE [master]
GO
/****** Object:  Database [SommerhusUdlejning]    Script Date: 25-02-2015 17:57:05 ******/
CREATE DATABASE [SommerhusUdlejning]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SommerhusUdlejning', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\SommerhusUdlejning.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SommerhusUdlejning_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\SommerhusUdlejning_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SommerhusUdlejning] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SommerhusUdlejning].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SommerhusUdlejning] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET ARITHABORT OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [SommerhusUdlejning] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SommerhusUdlejning] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SommerhusUdlejning] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SommerhusUdlejning] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SommerhusUdlejning] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET RECOVERY FULL 
GO
ALTER DATABASE [SommerhusUdlejning] SET  MULTI_USER 
GO
ALTER DATABASE [SommerhusUdlejning] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SommerhusUdlejning] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SommerhusUdlejning] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SommerhusUdlejning] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'SommerhusUdlejning', N'ON'
GO
USE [SommerhusUdlejning]
GO
/****** Object:  Table [dbo].[Lejer]    Script Date: 25-02-2015 17:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lejer](
	[UgeNr] [int] NOT NULL,
	[År] [int] NOT NULL,
	[Navn] [nchar](10) NULL,
	[Adresse] [nvarchar](50) NULL,
	[TelefonNr] [nchar](8) NULL,
	[Dato] [datetime] NULL,
	[Udlejet] [bit] NOT NULL,
 CONSTRAINT [PK_Lejer] PRIMARY KEY CLUSTERED 
(
	[UgeNr] ASC,
	[År] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
USE [master]
GO
ALTER DATABASE [SommerhusUdlejning] SET  READ_WRITE 
GO
