﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AllReservations.aspx.cs" Inherits="AllReservations" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <h3>Alle reservationer<asp:Button ID="btn_tilbage" runat="server" OnClick="Button1_Click" style="z-index: 1; left: 14px; top: 44px; position: absolute" Text="&lt;-- Tilbage" />
        </h3>
    
    </div>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="UgeNr,År" DataSourceID="SqlDataSourceDenRigtige" ForeColor="Black" GridLines="Vertical" style="z-index: 1; left: 13px; top: 103px; position: absolute; height: 133px; width: 664px">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="UgeNr" HeaderText="UgeNr" ReadOnly="True" SortExpression="UgeNr" />
                <asp:BoundField DataField="År" HeaderText="År" SortExpression="År" ReadOnly="True" />
                <asp:BoundField DataField="Navn" HeaderText="Navn" SortExpression="Navn" />
                <asp:BoundField DataField="Adresse" HeaderText="Adresse" SortExpression="Adresse" />
                <asp:BoundField DataField="TelefonNr" HeaderText="TelefonNr" SortExpression="TelefonNr" />
                <asp:BoundField DataField="Dato" HeaderText="Dato" SortExpression="Dato" />
                <asp:CheckBoxField DataField="Udlejet" HeaderText="Udlejet" SortExpression="Udlejet" />
            </Columns>
            <FooterStyle BackColor="#CCCC99" />
            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <RowStyle BackColor="#F7F7DE" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#FBFBF2" />
            <SortedAscendingHeaderStyle BackColor="#848384" />
            <SortedDescendingCellStyle BackColor="#EAEAD3" />
            <SortedDescendingHeaderStyle BackColor="#575357" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSourceDenRigtige" runat="server" ConnectionString="<%$ ConnectionStrings:SommerhusUdlejningConnectionString %>" SelectCommand="SELECT [UgeNr], [År], [Navn], [Adresse], [TelefonNr], [Dato], [Udlejet] FROM [Lejer] ORDER BY [År], [UgeNr]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SommerhusUdlejningConnectionString %>" SelectCommand="SELECT [ugenr], [Udlejet], [navn], [adresse], [Tlf], [Dato] FROM [Tabellen]"></asp:SqlDataSource>
           </form>
</body>
</html>
